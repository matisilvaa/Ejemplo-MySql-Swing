/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author heltonsmith
 */
public class Alumno {
    private String rut;
    private String nombre;
    private String apellido;
    
    private Connection con;     //conectar la bd
    private Statement state;    //ejecutar una query
    private ResultSet res;      //guarda una consulta select

    public Alumno(String rut, String nombre, String apellido) {
        try {
            this.rut = rut;
            this.nombre = nombre;
            this.apellido = apellido;
            
            Class.forName("com.mysql.jdbc.Driver"); //driver conexion
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ejemplomysqlswing?zeroDateTimeBehavior=convertToNull", "root", ""); //url, user, pass
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Alumno.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Alumno(String rut) {
        this.rut = rut;
    }

    public Alumno() {
        try {
            Class.forName("com.mysql.jdbc.Driver"); //driver conexion
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ejemplomysqlswing?zeroDateTimeBehavior=convertToNull", "root", ""); //url, user, pass
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Alumno.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public Statement getState() {
        return state;
    }

    public void setState(Statement state) {
        this.state = state;
    }

    public ResultSet getRes() {
        return res;
    }

    public void setRes(ResultSet res) {
        this.res = res;
    }
    
    public int insertar(){
        int saber = 0;
        try {

            String query = "insert into alumno values ('"+this.rut+"','"+this.nombre+"','"+this.apellido+"') ";
            
            state = con.createStatement();
            
            saber = state.executeUpdate(query); //insert update delete

            return saber;
        } catch (SQLException ex) {
            Logger.getLogger(Alumno.class.getName()).log(Level.SEVERE, null, ex);
        }
        return saber;
    }
    
    public Alumno buscar(String rut){
        
        Alumno a1 = null;
        
        try {
            String query = "select * from alumno where rut = '"+rut+"'  ";
            
            state = con.createStatement();
            
            res = state.executeQuery(query);
            
            while (res.next()) {                
                a1 = new Alumno(res.getString("rut"), res.getString("nombre"), res.getString("apellido"));
            }
         
            return a1;
            
        } catch (SQLException ex) {
            Logger.getLogger(Alumno.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return a1;
    }
    
    public ArrayList<Alumno> buscarTodos(){
        
        ArrayList<Alumno> alumnos = null;
        
        try {
            String query = "select * from alumno";
            
            state = con.createStatement();
            
            res = state.executeQuery(query);
            
            alumnos = new ArrayList<>();
            
            while (res.next()) {                
                alumnos.add(  new Alumno(res.getString("rut"), res.getString("nombre"), res.getString("apellido"))  );
            }
            
            return alumnos;
            
        } catch (SQLException ex) {
            Logger.getLogger(Alumno.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return alumnos;
    }
    
    
}
